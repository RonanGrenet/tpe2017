# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Installation du projet seineaxis pour GAMA ###

* Ouvrez Eclipse.

* Dans Eclipse, ouvrez la vue git.

* Dans la vue git, dans la section remote utiliser « create remote ... »

* Renseignez le nom de la branche, puis appuyez sur « OK ».

* Dans la fenêtre suivante cliquer sur « Change... », puis remplissez comme sur l'illustration
(URI : https://RonanGrenet@bitbucket.org/RonanGrenet/tpe2017.git), Puis appuyez sur « Finish ».

![Illustration du remplissage](https://bitbucket.org/RonanGrenet/tpe2017/downloads/uri2.png)

* Appuyez sur « Advanced... », puis dans « Source ref » séléctionnez la branche « master », puis cliquez sur « Add All Branches Spec », puis sur « Finish »

* Appuyez sur « Save ».

* Dans la vue git, cliquez sur « Push ».