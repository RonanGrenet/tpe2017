/*********************************************************************************************
 *
 * 'Startup.java, in plugin seineaxis.gama.ui.shared, is part of the source code of the GAMA modeling and simulation
 * platform. (c) 2007-2016 UMI 209 UMMISCO IRD/UPMC & Partners
 *
 * Visit https://github.com/gama-platform/gama for license information and developers contact.
 * 
 *
 **********************************************************************************************/
package seineaxis.gama.ui;

import org.eclipse.ui.IStartup;

import seineaxis.gama.ui.bindings.GamaKeyBindings;
import seineaxis.gama.ui.utils.CleanupHelper;

public class Startup implements IStartup {

	@Override
	public void earlyStartup() {
		CleanupHelper.run();
		GamaKeyBindings.install();
	}

}
