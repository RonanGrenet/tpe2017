/*********************************************************************************************
 *
 * 'ISpeedDisplayer.java, in plugin seineaxis.gama.ui.shared, is part of the source code of the
 * GAMA modeling and simulation platform.
 * (c) 2007-2016 UMI 209 UMMISCO IRD/UPMC & Partners
 *
 * Visit https://github.com/gama-platform/gama for license information and developers contact.
 * 
 *
 **********************************************************************************************/
package seineaxis.gama.ui.interfaces;

/**
 * Class ISpeedDisplayer.
 * 
 * @author drogoul
 * @since 29 mars 2014
 * 
 */
public interface ISpeedDisplayer {

	public void setInit(final double i, boolean notify);

}
